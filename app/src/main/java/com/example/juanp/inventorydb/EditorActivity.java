package com.example.juanp.inventorydb;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.juanp.inventorydb.data.InventoryContract.InventoryEntry;
import com.example.juanp.inventorydb.data.InventoryDbHelper;

public class EditorActivity extends AppCompatActivity {

    //Declaration of the editTexts
    private EditText productNameEditText;
    private EditText priceEditText;
    private EditText quantityEditText;
    private EditText supplierNameEditText;
    private EditText supplierPhoneNumberEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        //Set the editTexts
        productNameEditText = findViewById(R.id.editTextName);
        priceEditText = findViewById(R.id.editTextPrice);
        quantityEditText = findViewById(R.id.editTextQuantity);
        supplierNameEditText = findViewById(R.id.editTextSupplier);
        supplierPhoneNumberEditText = findViewById(R.id.editTextSupplierPhone);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Call insert method
            case R.id.action_save:
                //If statement to prevent bugs, if some field is empty the user cant save the info
                if (productNameEditText.getText().toString().isEmpty() || priceEditText.getText().toString().isEmpty() || quantityEditText.getText().toString().isEmpty() || supplierNameEditText.getText().toString().isEmpty() || supplierPhoneNumberEditText.getText().toString().isEmpty()) {
                    Toast.makeText(this, "You need to fill all the options", Toast.LENGTH_SHORT).show();
                }else {
                    insertItem();
                    finish();
                }
                return true;

            //Call back method
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Method that inserts the products
    private void insertItem() {

        //Get the input information
        String nameString = productNameEditText.getText().toString().trim();
        String priceString = priceEditText.getText().toString().trim();
        int price = Integer.parseInt(priceString);
        String quantityString = quantityEditText.getText().toString().trim();
        int quantity = Integer.parseInt(quantityString);
        String supplierNameString = supplierNameEditText.getText().toString().trim();
        String supplierPhoneString = supplierPhoneNumberEditText.getText().toString().trim();
        int phone = Integer.parseInt(supplierPhoneString);

        //Create a new db helper and set the info
        InventoryDbHelper mdbHelper = new InventoryDbHelper(this);
        SQLiteDatabase db = mdbHelper.getWritableDatabase();

        //Insert the values
        ContentValues values = new ContentValues();

        values.put(InventoryEntry.COLUMN_PRODUCT_NAME, nameString);
        values.put(InventoryEntry.COLUMN_PRICE, price);
        values.put(InventoryEntry.COLUMN_QUANTITY, quantity);
        values.put(InventoryEntry.COLUMN_SUPPLIER_NAME, supplierNameString);
        values.put(InventoryEntry.COLUMN_SUPPLIER_PHONE_NUMBER, phone);

        db.insert(InventoryEntry.TABLE_NAME, null, values);
    }
}