package com.example.juanp.inventorydb.data;

import android.provider.BaseColumns;

public final class InventoryContract {

    private InventoryContract() {}

    public static final class InventoryEntry implements BaseColumns {

        //Table name
        public final static String TABLE_NAME = "inventory";
        //ID
        public final static String _ID = BaseColumns._ID;
        //Name
        public final static String COLUMN_PRODUCT_NAME = "ProductName";
        //Price
        public final static String COLUMN_PRICE = "Price";
        //Quantity
        public final static String COLUMN_QUANTITY = "Quantity";
        //Supplier name
        public final static String COLUMN_SUPPLIER_NAME = "SupplierName";
        //Supplier Phone Number
        public final static String COLUMN_SUPPLIER_PHONE_NUMBER = "SupplierPhoneNumber";
    }
}