package com.example.juanp.inventorydb;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.juanp.inventorydb.data.InventoryContract.InventoryEntry;
import com.example.juanp.inventorydb.data.InventoryDbHelper;

public class MainActivity extends AppCompatActivity {

    //Dbhelper
    private InventoryDbHelper mDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set float add button
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, EditorActivity.class);
                startActivity(intent);
            }
        });
        // New instance of dbhelper
        mDbHelper = new InventoryDbHelper(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        displayDbInfo();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    //Method to show the database info
    private void displayDbInfo() {

        //Get the info
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        //String with the info
        String[] projection = {InventoryEntry._ID, InventoryEntry.COLUMN_PRODUCT_NAME, InventoryEntry.COLUMN_PRICE, InventoryEntry.COLUMN_QUANTITY, InventoryEntry.COLUMN_SUPPLIER_NAME, InventoryEntry.COLUMN_SUPPLIER_PHONE_NUMBER};

        //Cursor query to the db
        Cursor cursor = db.query(InventoryEntry.TABLE_NAME, projection, null, null, null, null, null);

        //Set textview
        TextView textShowInfo = findViewById(R.id.text_items);

        //Try to display the info
        try {

            textShowInfo.setText("The products table contains a total of " + cursor.getCount() + " items:\n\n");
            textShowInfo.append("ID" + " - " + InventoryEntry.COLUMN_PRODUCT_NAME + " - " + InventoryEntry.COLUMN_PRICE + " - " + InventoryEntry.COLUMN_QUANTITY + " - " + InventoryEntry.COLUMN_SUPPLIER_NAME + " - " + InventoryEntry.COLUMN_SUPPLIER_PHONE_NUMBER + "\n");

            //Get the info
            int idColumnIndex = cursor.getColumnIndex(InventoryEntry._ID);
            int productNameColumnIndex = cursor.getColumnIndex(InventoryEntry.COLUMN_PRODUCT_NAME);
            int priceColumnIndex = cursor.getColumnIndex(InventoryEntry.COLUMN_PRICE);
            int quantityColumnIndex = cursor.getColumnIndex(InventoryEntry.COLUMN_QUANTITY);
            int supplierNameColumnIndex = cursor.getColumnIndex(InventoryEntry.COLUMN_SUPPLIER_NAME);
            int supplierPhoneNumberColumnIndex = cursor.getColumnIndex(InventoryEntry.COLUMN_SUPPLIER_PHONE_NUMBER);

            //Bucle to set the info
            while(cursor.moveToNext()) {

                int currentID = cursor.getInt(idColumnIndex);
                String currentProductName = cursor.getString(productNameColumnIndex);
                int currentColumnPrice = cursor.getInt(priceColumnIndex);
                int currentQuantity = cursor.getInt(quantityColumnIndex);
                String currentSupplierName = cursor.getString(supplierNameColumnIndex);
                int currentSupplierPhoneNumber = cursor.getInt(supplierPhoneNumberColumnIndex);

                textShowInfo.append(("\n" + currentID + " - " + currentProductName + " - " + currentColumnPrice + " - " + currentQuantity + " - " + currentSupplierName + " - " + currentSupplierPhoneNumber));
            }
        } finally {
            cursor.close();
        }
    }
}