Project Overview

You're almost at the finish line! This project is the first part of a 2 stage project where you'll create a single amazing app 
that will pull together many of the components you've learned in this Nanodegree program!

In this project, you'll design and create the structure of a Inventory App that will allow a store to keep track of its inventory 
of products. The app will need to store information about the product and allow the user to track sales and shipments and make it 
easy for the user to order more from the listed supplier.

We will split the development of this app in two stages. Let's talk about Stage 1.

In this stage you’ll focus on what happens behind the scenes, practicing how to design and implement a simple database. 
Note: This stage of the project will not have any UI components